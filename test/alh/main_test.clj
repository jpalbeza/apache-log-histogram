(ns alh.main-test
  (:require [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [me.raynes.fs :as fs]
            [alh.main :refer :all]))

;; Just test the validation of the command line args here...

(defspec -main-with-invalid-http-codes-fail
  100
  (prop/for-all [http-code (gen/such-that (comp not valid-http-codes) gen/string-alphanumeric)]
                (let [out (with-out-str (-main http-code "examples"))]
                  (clojure.string/includes? out "Invalid HTTP Code."))))

(defspec -main-with-invalid-log-path-fails
  100
  (prop/for-all [path (gen/such-that (comp not fs/exists?) gen/string-alphanumeric)]
                (let [out (with-out-str (-main "404" path))]
                  (clojure.string/includes? out "Log file directory doesn't exist."))))


