(ns alh.example-test
  (:require [clojure.test :refer :all]
            [alh.main :refer :all]))

;; Demonstrative tests

(deftest example-001-test
  (testing "Example 001"
    (testing "with 404 code"
      (is (= (with-out-str (-main "404" "examples/001"))
"Http Code: 404
10 |
   |
   |
   |
   |
 5 |
   |     *
   | *   * *
   | * * * *
   | * * * *
   |-----------
     a b c d e

Service:
a: www.example.org-access.log
b: www1.example.org-access.log
c: www2.example.org-access.log
d: www3.example.org-access.log
e: www4.example.org-access.log
")))))

(deftest example-002-test
  (testing "Example 002"
    (testing "with 200 code"
      (is (= (with-out-str (-main "200" "examples/002"))
"Http Code: 200
100 |
    |
    |           *
    |           *
    |           * *
 50 | * *     * * *
    | * * *   * * *
    | * * * * * * *
    | * * * * * * *
    | * * * * * * *
    |---------------
      a b c d e f g

Service:
a: www.example.org-access.log
b: www1.example.org-access.log
c: www2.example.org-access.log
d: www3.example.org-access.log
e: www4.example.org-access.log
f: www5.example.org-access.log
g: www6.example.org-access.log
")))))

(deftest example-003-test
  (testing "Example 003"
    (testing "with 501 code"
      (is (= (with-out-str (-main "501" "examples/003"))
"Http Code: 501
1000 |
     |               * *
     | *         *   * *
     | * *       * * * *
     | * *       * * * *
 500 | * *     * * * * * *
     | * * *   * * * * * *
     | * * *   * * * * * *
     | * * *   * * * * * *
     | * * * * * * * * * *
     |---------------------
       a b c d e f g h i j

Service:
a: www.example.org-access.log
b: www1.example.org-access.log
c: www2.example.org-access.log
d: www3.example.org-access.log
e: www4.example.org-access.log
f: www5.example.org-access.log
g: www6.example.org-access.log
h: www7.example.org-access.log
i: www8.example.org-access.log
j: www9.example.org-access.log
")))))
