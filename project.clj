(defproject apache-logs-histogram "0.1.0-SNAPSHOT"
  :description "Display an ascii histogram displaying the number of requests with a given http code"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [me.raynes/fs "1.4.6"]
                 [clj-time "0.12.0"]
                 [org.clojure/test.check "0.9.0" :scope "test"]]

  :main alh.main)
