(ns alh.main
  (:require [me.raynes.fs :as fs]
            [alh.parse :as parse]
            [alh.histogram :as histogram])
  (:gen-class))

(defn print-histogram!
  [http-code log-file-directory]
  (->>
    (parse/log-directory->http-code-count http-code log-file-directory)
    (histogram/histogram-for-log-files http-code)
    (print)))

(defn print-usage!
  [error]
  (println "ERROR!" error)
  (println "Usage:")
  (println "lein run HTTP_CODE LOG_FILE_DIRECTORY")
  (println "Example:")
  (println "lein run 404 examples/001"))

(def valid-http-codes
  (->>
    (concat
      (range 100 103)
      (range 200 209)
      [226]
      (range 300 309)
      (range 400 418)
      (range 420 432)
      [451]
      (range 500 512))
    (map str)
    (set)))

(defn validate-args
  [& [http-code log-file-directory]]
  (cond
    (nil? (valid-http-codes http-code)) {:error "Invalid HTTP Code."}
    (not (fs/exists? log-file-directory)) {:error "Log file directory doesn't exist."}
    :else {:args [http-code log-file-directory]}))

(defn -main [& args]
  (let [{:keys [args error]} (apply validate-args args)]
    (if args
      (apply print-histogram! args)
      (print-usage! error))))
