(ns alh.histogram)

(defn- find-scale [max-item]
  (->> (iterate inc 1)
       (map #(apply * (repeat % 10)))
       (filter #(< max-item %))
       (take 1)
       first))

(defn- data-graph [sorted-vals scale label-width]
  (let [half-scale (quot scale 2)
        scale-unit (quot scale 10)]
    (loop [graph-str [] curr-scale scale]
      (if (zero? curr-scale)
        graph-str
        (recur (conj graph-str
                     (str (format (str "%" label-width "s |")
                                  (cond (= curr-scale scale) scale
                                        (= curr-scale half-scale) half-scale
                                        :else ""))
                          (->> sorted-vals
                               (map #(* scale-unit (Math/round (float (/ % scale-unit) ))))
                               (map #(if (>= % curr-scale) " *" "  "))
                               (reduce str)
                               clojure.string/trimr)
                          ))
               (- curr-scale scale-unit))))))


(defn histogram-for-log-files
  "Produce an ascii histogram from the m, for which:
  {filename(string) -> count-of-requests-with-http-code(int)}.
  An empty histogram will return string 'No files in directory.'"
  [http-code m]
  {:pre  [(map? m)]
   :post [(string? %)]}

  (if (empty? m)
    "No files in directory."
    (let [scale (find-scale (reduce max (vals m)))
          label-width (count (str scale))
          num-items (count m)
          [sorted-keys sorted-vals] (apply map list (into (sorted-map) m))
          non-empty-result
          (clojure.string/join
            "\n"
            (concat [(format "Http Code: %s" http-code)]    ;; header
                    (data-graph sorted-vals scale label-width)        ;; data-graph

                    ;; x-axis and beginning of legend
                    [(str (format (str "%" label-width "s |") "")
                          (apply str (repeat num-items "--"))
                          "-")
                     (str (format (str "%" label-width "s  ") "")
                          (apply str (->> (range num-items)
                                          (map #(str " " (char (+ 97 %)))))))
                     ""
                     "Service:"]

                    ;; legend
                    (->> sorted-keys
                         (map-indexed #(format "%s: %s" (char (+ 97 %1)) %2)))

                    [""]
                    ))
          ]
      non-empty-result)))